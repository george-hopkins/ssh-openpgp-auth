// SPDX-FileCopyrightText: 2024 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use std::array::TryFromSliceError;

use sequoia_openpgp::crypto::mpi::PublicKey as SequoiaPublicKey;
use sequoia_openpgp::packet::key::{KeyParts, KeyRole};
use sequoia_openpgp::packet::Key;
use sequoia_openpgp::types::Curve;
use ssh_key::public::PublicKey as SshPublicKey;
use ssh_key::public::{Ed25519PublicKey, KeyData, RsaPublicKey};
use ssh_key::Mpint;

pub struct PublicKey(SshPublicKey);

impl std::fmt::Display for PublicKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.0.to_openssh().expect("conversion to ssh to work")
        )
    }
}

impl<P: KeyParts, R: KeyRole> TryFrom<&Key<P, R>> for PublicKey {
    type Error = crate::Error;

    fn try_from(key: &sequoia_openpgp::packet::Key<P, R>) -> Result<Self, Self::Error> {
        let data = match key.mpis() {
            SequoiaPublicKey::EdDSA { curve, q } => match curve {
                Curve::Ed25519 => KeyData::Ed25519(Ed25519PublicKey(
                    q.decode_point(curve)?
                        .0
                        .try_into()
                        .map_err(|e: TryFromSliceError| crate::Error::Other(e.into()))?,
                )),
                _ => return Err(crate::Error::UnknownCurve(curve.clone(), key.fingerprint())),
            },
            SequoiaPublicKey::RSA { e, n } => KeyData::Rsa(RsaPublicKey {
                e: Mpint::from_positive_bytes(e.value())?,
                n: Mpint::from_positive_bytes(n.value())?,
            }),
            algo => {
                return Err(crate::Error::UnknownAlgorithm(
                    algo.clone(),
                    key.fingerprint(),
                ))
            }
        };
        Ok(Self(SshPublicKey::new(data, key.fingerprint().to_string())))
    }
}
