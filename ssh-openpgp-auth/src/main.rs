// SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use clap::Parser;
use ssh_openpgp_auth::{authenticate, Commands};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let cmds = Commands::parse();

    struct RealEffects;
    impl ssh_openpgp_auth::Effects for RealEffects {
        fn get(&mut self, url: String) -> Result<Vec<u8>, ssh_openpgp_auth::Error> {
            let response = reqwest::blocking::get(url)?;
            Ok(response.bytes()?.into())
        }

        fn dns_query(
            &mut self,
            nameserver: std::net::SocketAddr,
            name: &str,
        ) -> Result<Vec<String>, ssh_openpgp_auth::Error> {
            Ok(ssh_openpgp_auth::dns::dnssec_query(nameserver, name)?.collect())
        }
    }

    match cmds {
        Commands::Authenticate(auth) => {
            authenticate(auth, &mut RealEffects, &mut std::io::stdout().lock())?
        }
    }

    Ok(())
}
