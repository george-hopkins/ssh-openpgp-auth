// SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-FileCopyrightText: 2024 David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::io::Read;
use std::net::SocketAddr;
use std::path::PathBuf;
pub mod dns;
mod key;
use chrono::DateTime;
use chrono::Utc;
use clap::Parser;
use openpgp_cert_d::CertD;
use openpgp_cert_d::MergeResult;
use sequoia_cert_store::store::StoreError;
use sequoia_cert_store::CertStore;
use sequoia_openpgp::cert::prelude::*;
use sequoia_openpgp::crypto::mpi::PublicKey as SequoiaPublicKey;
use sequoia_openpgp::crypto::Signer;
use sequoia_openpgp::packet::signature::subpacket::NotationDataFlags;
use sequoia_openpgp::packet::signature::SignatureBuilder;
use sequoia_openpgp::packet::UserID;
use sequoia_openpgp::parse::Parse;
use sequoia_openpgp::policy::StandardPolicy;
use sequoia_openpgp::serialize::SerializeInto;
use sequoia_openpgp::types::Curve;
use sequoia_openpgp::types::RevocationStatus;
use sequoia_wot::store::CertStore as WotCertStore;
use sequoia_wot::{
    Network as WotNetwork, QueryBuilder as WotQueryBuilder, Roots as WotRoots, FULLY_TRUSTED,
};

#[derive(Debug, Parser)]
pub struct Authenticate {
    /// Target host to authenticate. This should be a DNS name.
    pub host: String,

    /// Certificate time. By default now.
    /// Example: 2021-11-21T11:11:11Z
    #[clap(short, long)]
    pub time: Option<DateTime<Utc>>,

    /// Certificate store. By default uses user's shared PGP certificate directory.
    #[clap(short, long, env = "PGP_CERT_D")]
    pub cert_store: Option<PathBuf>,

    /// Nameserver to use for DNS lookups (if enabled).
    #[clap(long, default_value = "8.8.8.8:53")]
    pub nameserver: SocketAddr,

    /// Verify Keyoxide DNS proofs of certificates.
    #[clap(long)]
    pub verify_dns_proof: bool,

    /// Verify the certificates using local Web of Trust network.
    #[clap(long)]
    pub verify_wot: bool,

    /// Print details of the verification process. Useful for debugging.
    #[clap(long)]
    pub verbose: bool,

    /// Store verification results in the OpenPGP certificate.
    #[clap(long)]
    pub store_verifications: bool,
}

impl Authenticate {
    fn verification_string(&self) -> String {
        let mut verifications = vec![];
        if self.verify_dns_proof {
            verifications.push("dns");
        }
        if self.verify_wot {
            verifications.push("wot");
        }
        verifications.join(" ")
    }
}

impl Default for Authenticate {
    fn default() -> Self {
        Self {
            host: "example.com".into(),
            time: None,
            cert_store: None,
            nameserver: "8.8.8.8:53".parse().expect("static value to be parseable"),
            verify_dns_proof: false,
            verify_wot: false,
            verbose: false,
            store_verifications: false,
        }
    }
}

#[derive(Debug, Parser)]
pub enum Commands {
    /// Fetches OpenPGP host certificates, verifies it and prints host keys in SSH format on successful verification.
    Authenticate(Authenticate),
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("OpenPGP error: {0}")]
    OpenPGP(#[from] anyhow::Error),

    #[error("I/O error: {0}")]
    IO(#[from] std::io::Error),

    #[error("Network error: {0}")]
    Network(#[from] reqwest::Error),

    #[error("SSH error: {0}")]
    Ssh(#[from] ssh_key::Error),

    #[error("Cert store error: {0}")]
    Store(#[from] openpgp_cert_d::Error),

    #[error("DNS error: {0}")]
    DnsProto(#[from] hickory_client::proto::error::ProtoError),

    #[error("DNS error: {0}")]
    DnsClient(#[from] hickory_client::error::ClientError),

    #[error("Cert store error: {0}")]
    CertStore(#[from] StoreError),

    #[error("Unknown curve: {0} in subkey: {1}")]
    UnknownCurve(Curve, sequoia_openpgp::Fingerprint),

    #[error("Unknown algorithm: {0:?} in subkey: {1}")]
    UnknownAlgorithm(SequoiaPublicKey, sequoia_openpgp::Fingerprint),

    #[error("Other error: {0}")]
    Other(Box<dyn std::error::Error>),
}

fn is_cert_expired(cert: ValidCert<'_>) -> bool {
    if let Some(expiry) = cert.primary_key().key_expiration_time() {
        cert.time() > expiry
    } else {
        // key without expiration is never expired
        false
    }
}

pub trait Effects {
    fn get(&mut self, url: String) -> Result<Vec<u8>, Error>;
    fn dns_query(
        &mut self,
        nameserver: SocketAddr,
        name: &str,
    ) -> Result<Vec<String>, crate::Error>;
}

pub fn authenticate(
    auth: Authenticate,
    effects: &mut impl Effects,
    writer: &mut impl std::io::Write,
) -> Result<(), Error> {
    let p = StandardPolicy::new();
    let store = if let Some(cert_store) = &auth.cert_store {
        CertD::with_base_dir(cert_store)?
    } else {
        CertD::new()?
    };
    let trust_root = if let Ok(Some((_, bytes))) = store.get("trust-root") {
        let trust_root_fpr = Cert::from_bytes(&bytes)?.fingerprint();
        if auth.verbose {
            writeln!(writer, "# Found trust root: {}", trust_root_fpr)?;
        }
        Some(trust_root_fpr)
    } else {
        None
    };
    let email = format!("ssh-openpgp-auth@{}", auth.host);
    let mut certs = vec![];
    for file in store.iter_files() {
        for cert in file.into_iter().flat_map(|(_, mut f)| {
            let mut v = Vec::new();
            f.read_to_end(&mut v)?;
            Cert::from_bytes(&v)
        }) {
            if let Ok(valid_cert) = cert.with_policy(&p, auth.time.map(Into::into)) {
                if valid_cert
                    .userids()
                    .any(|ui| ui.email2().unwrap_or_default() == Some(&email))
                    && !is_cert_expired(valid_cert)
                {
                    if auth.verbose {
                        writeln!(writer, "# Found local cert: {}", cert.fingerprint())?;
                    }
                    certs.push(cert);
                }
            }
        }
    }

    if certs.is_empty() {
        let f = |new: Cert, old: Option<&[u8]>| {
            if let Some(old) = old {
                let old = Cert::from_bytes(&old)?;
                let merged = old.merge_public(new)?.to_vec()?;

                Ok(MergeResult::Data(merged))
            } else {
                Ok(MergeResult::Data(new.to_vec()?))
            }
        };

        let bytes = effects.get(format!(
        "https://{}/.well-known/openpgpkey/hu/w1bjhwjfd8nqsw4ug3kn81sny45zimkq?l=ssh-openpgp-auth",
        auth.host
    ))?;

        let parser = CertParser::from_bytes(&*bytes)?;

        for cert in parser.flatten() {
            let fingerprint = &cert.fingerprint().to_string();
            if auth.verbose {
                writeln!(writer, "# Downloaded certificate: {}", fingerprint)?;
            }
            store.insert(fingerprint, cert.clone(), false, f)?;
            certs.push(cert);
        }
    }

    if auth.verify_dns_proof {
        let txts = effects.dns_query(auth.nameserver, &auth.host)?;

        let certs_and_validations = certs.into_iter().map(|cert| {
            (
                txts.contains(&format!("openpgp4fpr:{:x}", cert.fingerprint())),
                cert,
            )
        });

        let mut ok_certs = vec![];

        for (cert_in_dns, cert) in certs_and_validations {
            if auth.verbose {
                writeln!(
                    writer,
                    "# Certificate {}{} found in the DNS zone",
                    cert.fingerprint(),
                    if cert_in_dns { "" } else { " NOT" }
                )?;
            }
            if cert_in_dns {
                ok_certs.push(cert);
            }
        }

        certs = ok_certs;
    }

    if auth.verify_wot {
        let mut store = CertStore::empty();
        if let Some(cert_store) = &auth.cert_store {
            if auth.verbose {
                writeln!(writer, "# Using cert store: {}", cert_store.display())?;
            }
            store.add_certd(cert_store)?;
        } else {
            if auth.verbose {
                writeln!(writer, "# Using default cert store")?;
            }
            store.add_default_certd()?;
        };
        let store = WotCertStore::from_store(&store, &p, auth.time.map(Into::into));
        let network = WotNetwork::new(&store)?;
        let mut builder = WotQueryBuilder::new(&network);
        if let Some(fingerprint) = trust_root {
            builder.roots(WotRoots::new([fingerprint]));
        }
        let query = builder.build();
        let user_id = UserID::from(format!("<ssh-openpgp-auth@{}>", auth.host));
        let valid_certs = certs.into_iter().map(|cert| {
            let paths = query.authenticate(&user_id, cert.fingerprint(), FULLY_TRUSTED);
            (cert, paths.amount() > 0)
        });
        let mut new_certs = vec![];
        for (cert, valid) in valid_certs {
            if auth.verbose {
                writeln!(
                    writer,
                    "# Web of Trust verification of {} {}",
                    cert.fingerprint(),
                    if valid { "succeeded" } else { "failed" }
                )?;
            }
            if valid {
                new_certs.push(cert);
            }
        }
        certs = new_certs;
    }

    let mut persistence_cert: Option<Box<dyn Signer>> = if auth.store_verifications {
        Some(
            if let Some((_, bytes)) = store.get("_metacode_ssh_openpgp_auth_persistence.pgp")? {
                Cert::from_bytes(&bytes)
            } else {
                Ok(create_new_certifying_key(&store)?)
            }
            .and_then(|cert| {
                Ok(Box::new(
                    cert.primary_key()
                        .key()
                        .clone()
                        .parts_into_secret()?
                        .into_keypair()?,
                ) as Box<dyn Signer>)
            })?,
        )
    } else {
        None
    };

    for cert in certs {
        let cert = cert.with_policy(&p, auth.time.map(Into::into))?;
        if cert.alive().is_err() {
            if auth.verbose {
                writeln!(
                    writer,
                    "# Certificate {} is already expired at {}",
                    cert.fingerprint(),
                    auth.time.unwrap_or_default()
                )?;
            }
        } else if cert.revocation_status() != RevocationStatus::NotAsFarAsWeKnow {
            if auth.verbose {
                writeln!(
                    writer,
                    "# Certificate {} is skipped because it is revoked",
                    cert.fingerprint(),
                )?;
            }
        } else {
            for key in cert.keys().for_authentication() {
                match key.revocation_status() {
                    RevocationStatus::NotAsFarAsWeKnow => {
                        if auth.verbose {
                            writeln!(
                                writer,
                                "# Certificate {}, exporting subkey {}",
                                cert.fingerprint(),
                                key.fingerprint()
                            )?;
                        }
                        match key::PublicKey::try_from(key.key()) {
                            Ok(key) => writeln!(writer, "{} {}", auth.host, key)?,
                            Err(error) => writeln!(writer, "# {}", error)?,
                        }
                    }
                    RevocationStatus::Revoked(_revocations)
                    | RevocationStatus::CouldBe(_revocations) => {
                        if auth.verbose {
                            writeln!(
                                writer,
                                "# Certificate {}, skipping subkey {} as it is revoked",
                                cert.fingerprint(),
                                key.fingerprint()
                            )?;
                        }
                    }
                }
            }
        }

        if let Some(ref mut persistence_cert) = persistence_cert {
            let signature =
                SignatureBuilder::new(sequoia_openpgp::types::SignatureType::PositiveCertification)
                    .add_notation(
                        "ssh-openpgp-auth-verification@metacode.biz",
                        auth.verification_string(),
                        NotationDataFlags::empty().set_human_readable(),
                        false,
                    )?
                    .set_exportable_certification(false)?
                    .sign_userid_binding(
                        persistence_cert,
                        cert.primary_key().key(),
                        &UserID::from(format!("<ssh-openpgp-auth@{}>", auth.host)),
                    )?;

            let new_cert = cert.cert().clone().insert_packets2(signature)?.0;
            store.insert(
                &cert.fingerprint().to_string(),
                new_cert.clone(),
                false,
                |new, old| {
                    Ok(match old {
                        Some(old) => {
                            let old = Cert::from_bytes(&old)?;
                            MergeResult::Data(old.merge_public(new)?.as_tsk().to_vec()?)
                        }
                        None => MergeResult::Data(new.as_tsk().to_vec()?),
                    })
                },
            )?;
        }
    }
    Ok(())
}

/// Creates and inserts a new certifying key which is used to add proof
/// verification results to host certificates.
///
/// This function uses CertD optimistic-locking to insert the certificate
/// and, if it already exists, return the old one.
fn create_new_certifying_key(store: &CertD) -> sequoia_openpgp::Result<Cert> {
    let cert = CertBuilder::new().generate()?.0;
    let cert_data = store.insert_special(
        "_metacode_ssh_openpgp_auth_persistence.pgp",
        cert.clone(),
        true,
        // if the certificate has already been created (before the previous check
        // and calling this function) just take the existing one and throw
        // away the generated one
        |new, old| {
            Ok(match old {
                Some(old) => MergeResult::Data(old.into()),
                None => MergeResult::Data(new.as_tsk().to_vec()?),
            })
        },
    )?;
    if let Some(cert_data) = cert_data.1 {
        Cert::from_bytes(&cert_data)
    } else {
        Ok(cert)
    }
}
