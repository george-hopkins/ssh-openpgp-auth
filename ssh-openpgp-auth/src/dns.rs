// SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::net::SocketAddr;
use std::str::FromStr;

use hickory_client::rr::RData;
use hickory_client::{
    client::Client,
    rr::{DNSClass, Name, RecordType},
};

pub fn dnssec_query(
    nameserver: SocketAddr,
    name: &str,
) -> Result<impl Iterator<Item = String>, crate::Error> {
    let client = hickory_client::client::SyncDnssecClient::new(
        hickory_client::tcp::TcpClientConnection::new(nameserver)?,
    )
    .build();
    let mut results = client
        .query(&Name::from_str(name)?, DNSClass::IN, RecordType::TXT)?
        .into_message();
    Ok(results.take_answers().into_iter().flat_map(|answer| {
        if let Some(RData::TXT(data)) = answer.data() {
            let bytes = data
                .txt_data()
                .iter()
                .fold(Vec::new(), |mut v: Vec<u8>, n| {
                    v.extend(&n[..]);
                    v
                });
            Some(String::from_utf8_lossy(&bytes).to_string())
        } else {
            None
        }
    }))
}
