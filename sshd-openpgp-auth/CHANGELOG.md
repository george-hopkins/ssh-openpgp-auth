<!--
# SPDX-FileCopyrightText: 2024 David Runge <dave@sleepmap.de>
# SPDX-FileCopyrightText: 2024 Wiktor Kwapisiewicz <wiktor@metacode.biz>
# SPDX-License-Identifier: CC0-1.0
-->
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0](https://codeberg.org/wiktor/ssh-openpgp-auth/compare/sshd-openpgp-auth-v0.2.1...sshd-openpgp-auth-v0.3.0) - 2024-02-29

### Added
- *(sshd-openpgp-auth)* [**breaking**] Use .tsk as file ending for trust anchors
- *(sshd-openpgp-auth)* Add command for merging certificates

### Fixed
- *(sshd-openpgp-auth)* Also extend expiry of already expired TSKs
- *(sshd-openpgp-auth)* Remove unneeded stdout prints on export to WKD
- Replace use of sequoia-net with local implementation

### Other
- *(README.md)* Use correct file ending in examples
- *(README.md)* Add section on adding thirdparty certifications to TSK
- Rename fixture to clarify that it contains a trust anchor
- *(sshd-openpgp-auth)* Use ignore instead of feature for online test
- Use NLNet's template in the "Funding/Sponsors" section

## [0.2.1](https://codeberg.org/wiktor/ssh-openpgp-auth/compare/sshd-openpgp-auth-v0.2.0...sshd-openpgp-auth-v0.2.1) - 2024-02-10

### Other
- update Cargo.lock dependencies
