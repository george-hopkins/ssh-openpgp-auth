// SPDX-FileCopyrightText: 2024 David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT

use rstest::rstest;
use sequoia_openpgp::Cert;

mod common;
use common::trust_anchor_ca;
use common::trust_anchor_with_subkeys;
use common::Error;
use sequoia_openpgp::packet::UserID;
use sequoia_openpgp::policy::StandardPolicy;
use sequoia_openpgp::types::SignatureType;
use sshd_openpgp_auth::merge_public_cert;
use testresult::TestResult;

#[rstest]
fn test_add_certification_to_tsk(
    trust_anchor_with_subkeys: Result<Cert, Error>,
    trust_anchor_ca: Result<Cert, Error>,
) -> TestResult {
    let tsk = trust_anchor_with_subkeys?;
    let updated_trust_anchor_cert = tsk.clone();
    let trust_anchor_ca = trust_anchor_ca?;
    let policy = StandardPolicy::new();

    // certify trust anchor
    let mut keypair = trust_anchor_ca
        .primary_key()
        .key()
        .clone()
        .parts_into_secret()?
        .into_keypair()?;
    let primary_userid: UserID = updated_trust_anchor_cert
        .with_policy(&policy, None)?
        .primary_userid()?
        .userid()
        .clone();
    let certification = primary_userid.certify(
        &mut keypair,
        &updated_trust_anchor_cert,
        SignatureType::PositiveCertification,
        None,
        None,
    )?;
    // add certification to updated version of trust anchor certificate
    let cert = updated_trust_anchor_cert.insert_packets([certification])?;

    let updated_tsk = merge_public_cert(tsk, cert)?;
    assert_eq!(
        updated_tsk
            .with_policy(&policy, None)?
            .primary_userid()?
            .certifications()
            .count(),
        1
    );

    Ok(())
}
