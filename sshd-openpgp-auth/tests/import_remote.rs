// SPDX-FileCopyrightText: 2023-2024 David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::str::FromStr;

use assert_cmd::Command;
use fqdn::FQDN;
use rstest::rstest;
use sshd_openpgp_auth::parse_known_hosts;
use testresult::TestResult;

mod common;
use common::ssh_known_hosts;
use common::Error;

#[rstest]
fn test_parse_known_hosts(ssh_known_hosts: Result<String, Error>) -> TestResult {
    let ssh_public_keys = parse_known_hosts(&ssh_known_hosts?)?;
    println!("{:?}", ssh_public_keys);
    assert!(ssh_public_keys.len() == 3);
    Ok(())
}

#[ignore = "requires access to the internet"]
#[rstest]
fn test_scan_remote_keys() -> TestResult {
    let known_hosts = String::from_utf8(
        Command::new("ssh-keyscan")
            .arg("-t")
            .arg("ecdsa,ed25519,rsa")
            .arg(format!("{}", FQDN::from_str("github.com")?))
            .output()?
            .stdout,
    )?;

    let ssh_public_keys = parse_known_hosts(&known_hosts)?;
    println!("{:?}", ssh_public_keys);
    assert!(ssh_public_keys.len() == 3);
    Ok(())
}
