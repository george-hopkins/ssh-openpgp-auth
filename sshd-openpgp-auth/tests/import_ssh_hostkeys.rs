// SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
// SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::path::PathBuf;
use std::time::SystemTime;

use rstest::rstest;
use sequoia_openpgp::packet::key::PublicParts;
use sequoia_openpgp::packet::key::SubordinateRole;
use sequoia_openpgp::packet::Key;
use sequoia_openpgp::Cert;
use sshd_openpgp_auth::attach_subkeys_to_cert;
use sshd_openpgp_auth::create_openpgp_subkey_from_ssh_public_key_file;
use sshd_openpgp_auth::get_public_ssh_host_keys;
use testdir::testdir;
use testresult::TestResult;

mod common;
use common::ssh_config_dir;
use common::ssh_public_host_keys;
use common::subkeys;
use common::trust_anchor;
use common::Error;

#[rstest]
fn test_get_public_ssh_host_keys_succeeds(ssh_config_dir: Result<PathBuf, Error>) -> TestResult {
    assert!(get_public_ssh_host_keys(Some(ssh_config_dir?.as_path())).is_ok());
    Ok(())
}

#[rstest]
fn test_get_public_ssh_host_keys_empty() -> TestResult {
    assert!(get_public_ssh_host_keys(Some(testdir!().as_path())).is_ok_and(|x| x.is_empty()));
    Ok(())
}

#[rstest]
fn test_create_subkey_from_ssh_host_key(
    ssh_public_host_keys: Result<Vec<PathBuf>, Error>,
) -> TestResult {
    for key in ssh_public_host_keys?.iter() {
        println!("public key: {}", key.display());
        let subkey =
            create_openpgp_subkey_from_ssh_public_key_file(key.as_path(), Some(SystemTime::now()));
        println!("{:?}", subkey);
        assert!(subkey.is_ok());
    }
    Ok(())
}

#[rstest]
fn test_attach_subkeys_to_cert(
    trust_anchor: Result<Cert, Error>,
    subkeys: Result<Vec<Key<PublicParts, SubordinateRole>>, Error>,
) -> TestResult {
    let subkeys = subkeys?;
    let subkeys_len = subkeys.len();

    let cert = attach_subkeys_to_cert(trust_anchor?, subkeys)?;
    assert!(subkeys_len == cert.keys().subkeys().count());

    Ok(())
}
