// SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::time::Duration;
use std::time::SystemTime;

use rstest::rstest;
use sequoia_openpgp::policy::Policy;
use sequoia_openpgp::policy::StandardPolicy;
use sequoia_openpgp::Cert;
use sshd_openpgp_auth::create_trust_anchor;
use sshd_openpgp_auth::extend_expiry_of_cert;
use sshd_openpgp_auth::EXPIRY_PERIOD;
use sshd_openpgp_auth::EXPIRY_THRESHOLD;
use testresult::TestResult;

mod common;
use common::default_host;
use common::Error;

fn create_custom_trust_anchor(validity_period: Option<Duration>) -> Result<Cert, Error> {
    Ok(create_trust_anchor(
        &default_host()?,
        None,
        validity_period,
    )?)
}

fn get_expiration_time(
    cert: Cert,
    policy: Option<&dyn Policy>,
    reference_time: Option<SystemTime>,
) -> Result<SystemTime, Error> {
    let default_policy = StandardPolicy::new();
    let policy = policy.unwrap_or(&default_policy);
    let reference_time = reference_time.unwrap_or(SystemTime::now());
    Ok(cert
        .with_policy(policy, reference_time)?
        .primary_key()
        .key_expiration_time()
        .unwrap_or(SystemTime::now()))
}

#[rstest]
#[case(Some(Duration::new(365 * 24 * 60 * 60, 0)), Some(EXPIRY_THRESHOLD), Some(EXPIRY_PERIOD), false)]
#[case(Some(Duration::new(200 * 24 * 60 * 60, 0)), Some(EXPIRY_THRESHOLD), Some(EXPIRY_PERIOD), true)]
fn test_extend_expiry_of_primary_key(
    #[case] cert_validity_period: Option<Duration>,
    #[case] expiry_threshold: Option<Duration>,
    #[case] expiry_period: Option<Duration>,
    #[case] expiry_should_be_updated: bool,
) -> TestResult {
    let reference_time = SystemTime::now();

    let cert = create_custom_trust_anchor(cert_validity_period)?;
    let updated_cert = extend_expiry_of_cert(
        cert.clone(),
        expiry_threshold,
        expiry_period,
        Some(reference_time),
    )?;

    let check_time = reference_time.checked_add(Duration::new(120, 0));

    let expiration_time_before = get_expiration_time(cert.clone(), None, check_time)?;
    let expiration_time_after = get_expiration_time(updated_cert.clone(), None, check_time)?;
    println!("expiry before: {:?}", expiration_time_before);
    println!("expiry after: {:?}", expiration_time_after);
    println!("cert before: #({:#?})", cert);
    println!("cert after: #({:#?})", updated_cert);
    assert_eq!(
        expiration_time_before != expiration_time_after,
        expiry_should_be_updated
    );
    Ok(())
}
