// SPDX-FileCopyrightText: 2024 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use rstest::rstest;
use sequoia_openpgp::{cert::CertBuilder, policy::StandardPolicy, Cert};
use sshd_openpgp_auth::add_dns_proof;
use testresult::TestResult;

fn get_notation(cert: &Cert) -> Result<Option<String>, testresult::TestError> {
    let p = StandardPolicy::new();
    let va = cert.with_policy(&p, None)?;
    let pui = va.primary_userid()?;
    let sig = pui.binding_signature(&p, None)?;
    let mut values = sig.notation("proof@ariadne.id");
    if let Some(value) = values.next() {
        let value = String::from_utf8_lossy(value).to_string();
        // make sure there are no more notations with this name
        assert_eq!(values.next(), None);
        Ok(Some(value))
    } else {
        Ok(None)
    }
}

#[rstest]
fn check_adding_proofs() -> TestResult {
    let cert = CertBuilder::new()
        .add_userid("<ssh-openpgp-auth@example.com>")
        .generate()?
        .0;
    assert_eq!(get_notation(&cert)?, None);
    std::thread::sleep(std::time::Duration::from_secs(2));
    let cert = add_dns_proof(cert)?;
    assert_eq!(
        get_notation(&cert)?,
        Some("dns:example.com?type=TXT".into())
    );
    Ok(())
}
