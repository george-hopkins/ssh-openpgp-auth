// SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::time::Duration;
use std::time::SystemTime;

use rstest::rstest;
use sequoia_openpgp::policy::StandardPolicy;
use sequoia_openpgp::types::RevocationStatus;
use sequoia_openpgp::Cert;
use sequoia_openpgp::Fingerprint;
use sshd_openpgp_auth::revoke_subkey_of_cert;
use testresult::TestResult;

mod common;
use common::trust_anchor_with_subkeys;
use common::Error;

#[rstest]
fn test_revoke_subkey_of_cert(trust_anchor_with_subkeys: Result<Cert, Error>) -> TestResult {
    let cert = trust_anchor_with_subkeys?;
    let input_cert = cert.clone();
    let fingerprints: Vec<Fingerprint> = cert
        .keys()
        .subkeys()
        .map(|subkey| subkey.fingerprint().clone())
        .collect();
    let output_cert = revoke_subkey_of_cert(input_cert, fingerprints, None, None, None)?;

    assert!(output_cert.is_tsk());

    let valid_subkeys = output_cert
        .keys()
        .subkeys()
        .filter_map(|subkey| {
            (&subkey.revocation_status(
                &StandardPolicy::new(),
                SystemTime::now() + Duration::new(10, 0),
            ) == &RevocationStatus::NotAsFarAsWeKnow)
                .then_some(subkey.fingerprint().clone())
        })
        .count();
    assert!(valid_subkeys == 0);
    Ok(())
}
