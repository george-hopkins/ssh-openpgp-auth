// SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::path::PathBuf;
use std::time::SystemTime;

use fqdn::FQDN;
use rstest::rstest;
use sequoia_openpgp::Cert;
use sshd_openpgp_auth::export_certs_to_wkd;
use sshd_openpgp_auth::WkdType;
use testdir::testdir;
use testresult::TestResult;

mod common;
use common::default_host;
use common::trust_anchor_with_subkeys;
use common::wrong_host;
use common::Error;

#[rstest]
#[case(true, WkdType::Advanced, testdir!(), SystemTime::now())]
#[case(true, WkdType::Direct, testdir!(), SystemTime::now())]
#[case(false, WkdType::Advanced, testdir!(), SystemTime::now())]
#[case(false, WkdType::Direct, testdir!(), SystemTime::now())]
fn test_export_to_wkd(
    trust_anchor_with_subkeys: Result<Cert, Error>,
    default_host: Result<FQDN, Error>,
    wrong_host: Result<FQDN, Error>,
    #[case] valid_fqdn: bool,
    #[case] wkd_type: WkdType,
    #[case] output_dir: PathBuf,
    #[case] reference_time: SystemTime,
) -> TestResult {
    let fqdn = if valid_fqdn {
        default_host?
    } else {
        wrong_host?
    };
    let certs = vec![trust_anchor_with_subkeys?];
    export_certs_to_wkd(
        certs,
        fqdn.clone(),
        wkd_type,
        &output_dir,
        Some(reference_time),
    )?;

    let (policy_file, hu_dir) = match wkd_type {
        WkdType::Advanced => (
            output_dir.clone().join(PathBuf::from(format!(
                ".well-known/openpgpkey/{}/policy",
                fqdn
            ))),
            output_dir
                .clone()
                .join(PathBuf::from(format!(".well-known/openpgpkey/{}/hu", fqdn))),
        ),
        WkdType::Direct => (
            output_dir
                .clone()
                .join(PathBuf::from(".well-known/openpgpkey/policy")),
            output_dir
                .clone()
                .join(PathBuf::from(".well-known/openpgpkey/hu")),
        ),
    };

    // ensure that relevant files and directories are created if the FQDN matches (and none are created if it does not)
    if valid_fqdn {
        assert!(policy_file.exists() && policy_file.is_file());
        assert!(hu_dir.exists() && hu_dir.is_dir());
        assert!(output_dir.read_dir().is_ok_and(|x| x.count() == 1));
    } else {
        assert!(!policy_file.exists() && !policy_file.is_file());
        assert!(!hu_dir.exists() && !hu_dir.is_dir());
    }
    Ok(())
}
