# SPDX-FileCopyrightText: 2024 Doron Behar <doron.behar@gmail.com>
# SPDX-License-Identifier: CC0-1.0
{ lib
, rustPlatform
, pname, cargoTomlPkg, src
, pkg-config
, just
, rust-script
, installShellFiles
, bzip2
, nettle
, openssl
, sqlite
, stdenv
, darwin
, openssh
}:

rustPlatform.buildRustPackage {
  inherit pname;
  inherit (cargoTomlPkg) version;
  inherit src;
  buildAndTestSubdir = pname;

  cargoLock = {
    lockFile = ./Cargo.lock;
  };

  nativeBuildInputs = [
    pkg-config
    rustPlatform.bindgenHook
    just
    rust-script
    installShellFiles
  ];
  # Otherwise just's build, check and install phases take precedence over
  # buildRustPackage's phases.
  dontUseJustBuild = true;
  dontUseJustCheck = true;
  dontUseJustInstall = true;

  postInstall = ''
    export HOME=$(mktemp -d)
    just generate manpages ${pname} $out/share/man/man1
    just generate shell_completions ${pname} shell_completions
    installShellCompletion --cmd ${pname} \
      --bash shell_completions/${pname}.bash \
      --fish shell_completions/${pname}.fish \
      --zsh  shell_completions/_${pname}
  '';

  buildInputs = [
    nettle
    openssl
    sqlite
  ] ++ lib.optionals stdenv.isDarwin [
    darwin.apple_sdk_11_0.frameworks.CoreFoundation
    darwin.apple_sdk_11_0.frameworks.IOKit
    darwin.apple_sdk_11_0.frameworks.Security
    darwin.apple_sdk_11_0.frameworks.SystemConfiguration
  ];

  doCheck = true;
  nativeCheckInputs = [
    openssh
  ];

  meta = with lib; {
    inherit (cargoTomlPkg)
      description
      homepage
    ;
    downloadPage = cargoTomlPkg.repository;
    license = with licenses; [ mit /* or */ asl20 ];
    maintainers = with maintainers; [ doronbehar ];
    mainProgram = "ssh-openpgp-auth";
  };
}
