<!--
SPDX-FileCopyrightText: 2024- Wiktor Kwapisiewicz <wiktor@metacode.biz>
SPDX-License-Identifier: CC0-1.0
-->

# OpenPGP CA integration

Although SSH OpenPGP Authenticator does not explicitly integrate with [OpenPGP CA](https://openpgp-ca.org/), the two projects interoperate seamlessly because they share OpenPGP certificates as a common format. Organizations that already use OpenPGP CA can seamlessly introduce Authenticator.

For the purposes of this guide, we will assume that the `OPENPGP_CA_DB` environment variable points to the state directory of OpenPGP CA.

## Initialization

This guide assumes that the host certificate has already been initialized:

```sh
sshd-openpgp-auth init example.com
```

The OpenPGP CA instance needs to be initialized as well:

```sh
oca ca init --domain example.com softkey
```

```
Initialized OpenPGP CA instance:

    CA Domain: example.com
  Fingerprint: FC278B891DAB11ADB658A578C8F94DE462FA2E16
Creation time: 2024-02-21 13:52:56 UTC
   CA Backend: Softkey (private key material in CA database)
```

Now the host certificate can be imported into the CA database:

```sh
sshd-openpgp-auth export example.com
oca user import --email ssh-openpgp-auth@example.com --key-file wkd/.well-known/openpgpkey/example.com/hu/w1bjhwjfd8nqsw4ug3kn81sny45zimkq
```

Using OpenPGP CA, the entire userbase can then be exported into a separate WKD directory:

```sh
oca wkd export /tmp/wkd
```

The export contains both certificates: The organization's OpenPGP CA certificate and the SSH host certificate:

```sh
$ tree -aC /tmp/wkd
/tmp/wkd
└── .well-known
    └── openpgpkey
        └── example.com
            ├── hu
            │   ├── ermf4k8pujzwtqqxmskb7355sebj5e4t
            │   └── w1bjhwjfd8nqsw4ug3kn81sny45zimkq
            └── policy
```

The host certificate in this WKD export contains a certification (also known as a "third-party signature") issued by the organization's OpenPGP CA certificate.

It is highly advisable to merge the exported certificates back into `sshd-openpgp-auth` store to preserve the newly issued certifications:

```sh
sshd-openpgp-auth merge `/tmp/wkd/.well-known/openpgpkey/example.com/hu/*"
```

Now, `sshd-openpgp-auth export` will also include OpenPGP CA certifications.

## End-user

The end-user imports the OpenPGP CA certificate (here, having the fingerprint `$CA_FPR`) and [marks it as a trusted introducer](https://sequoia-pgp.org/blog/2023/04/08/sequoia-sq/#an-address-book-style-trust-model) for the organization's domain:

```sh
sq network wkd fetch "openpgp-ca@example.com"
sq pki link add --ca example.com $CA_FPR openpgp-ca@example.com
```

The `sq pki link add` command displays the following success message:

```
Linking FC278B891DAB11ADB658A578C8F94DE462FA2E16 and "OpenPGP CA <openpgp-ca@example.com>".
```

This is a one-time action, which causes all current and future host certificates to be trusted by `ssh-openpgp-auth`:

```
$ ssh-openpgp-auth authenticate --verbose --verify-wot example.com
# Found trust root: 863D6704479ED10029129002CE7063AFB14C5D8B
# Found local cert: 04F0A81458FDB7CDD8D3FC96B613107E53224CAF
# Using default cert store
# Web of Trust verification of 04F0A81458FDB7CDD8D3FC96B613107E53224CAF succeeded
example.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN1KLfPT949Gq15XcaTkxFntkp6fFyoNq0JkPOKaktJM F5CEDEED08E9EA536034F5823475162385DF08AF
```
