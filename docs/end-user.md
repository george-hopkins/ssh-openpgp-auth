<!-- SPDX-FileCopyrightText: 2024- Wiktor Kwapisiewicz <wiktor@metacode.biz>
SPDX-License-Identifier: CC0-1.0
-->

# End-user's guide

SSH OpenPGP Authenticator requires minimal interaction from the end-user and tries to seamlessly integrate into existing tools that may be installed.

This guide assumes that you already have `ssh-openpgp-auth` installed, either as a distribution package, or manually via `cargo install ssh-openpgp-auth`.
The distribution's package is a preferred mechanism, due to updates and tighter integration with the operating system through manpages and shell completions.

## Setup

Only a minimal change to `~/.ssh/config` is required to make OpenSSH's `ssh` use SSH OpenPGP Authenticator.
Add the below to your ssh configuration file (create the `~/.ssh/config` file and the parent directory if it does not exist yet):

```
Host example.com
    KnownHostsCommand /usr/bin/ssh-openpgp-auth authenticate %H
```

The `Host` expression can be used to target a specific host (here `example.com`) or to match all hosts (`*` - star). Further information on the `Host` expression can be found in [`man 5 ssh_config`](https://man.archlinux.org/man/core/openssh/ssh_config.5.en#Host).

## Stronger verification

For most setups the configuration change listed above is enough.
Organizations already utilizing Web of Trust, e.g. via [OpenPGP CA](https://openpgp-ca.org/) may want to enable extended authentication.

To enable validation based on the Web of Trust the additional flag `--verify-wot` needs to be added:

```
Host example.com
    KnownHostsCommand /usr/bin/ssh-openpgp-auth authenticate --verify-wot %H
```

Validation based on the Web of Trust requires trusting either the host certificate directly or an organization's certificate (when using [OpenPGP CA](https://openpgp-ca.org/)).

Let us imagine, the OpenPGP certificate with the fingerprint `$FINGERPRINT` serves as the trusted introducer (Certificate Authority (CA)).
Users can link this OpenPGP certificate to be used as CA for an entire given domain:

```sh
sq pki link add --ca "example.com" "$FINGERPRINT" "openpgp-ca@example.com"
```

To trust a host's certificate directly, certify the relevant user id: `<ssh-openpgp-auth@example.com>`:

```sh
sq pki link add --userid "<ssh-openpgp-auth@example.com>" "$FINGERPRINT"
```

## Troubleshooting

Enabling additional authentication options can result in authentication failures.
For cases when additional data about the authentication process is needed a `--verbose` flag can be appended to the command line.
`--verbose` can be combined with any other flags and the rest of the command-line should remain the same.

An example of getting verbose output for extended verification using the Web of Trust:

```sh
ssh-openpgp-auth authenticate --verbose --verify-wot example.com
```

Then the actual application output will contain all details about the verification process:

```
# Found trust root: CEDEED08E9EA536034F5823475162385DF08AF5F
# Found local cert: D9E95D7F42E87610676C40B47E8432836DA1625E
# Using cert store: /home/wiktor/.local/share/pgp.cert.d
# Web of Trust verification of D9E95D7F42E87610676C40B47E8432836DA1625E succeeded
# Certificate D9E95D7F42E87610676C40B47E8432836DA1625E, exporting subkey F5CEDEED08E9EA536034F5823475162385DF08AF
example.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN1KLfPT949Gq15XcaTkxFntkp6fFyoNq0JkPOKaktJM F5CEDEED08E9EA536034F5823475162385DF08AF
```

The above output provides commented output (lines prefixed with `# `), that outline the inner workings of `ssh-openpgp-auth`, to provide context about why authentication may fail.
Only uncommented lines (those not prefixed with `# `) are used by the OpenSSH client and in the case of successful authentication contain an entry in [known_hosts file format](https://man.archlinux.org/man/sshd.8.en#SSH_KNOWN_HOSTS_FILE_FORMAT).
