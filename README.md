<!--
SPDX-FileCopyrightText: 2024- Wiktor Kwapisiewicz <wiktor@metacode.biz>
SPDX-License-Identifier: MIT OR Apache-2.0
-->

# SSH OpenPGP Authenticator

This project aims to improve the security of SSH connections by providing a way to verify host keys using OpenPGP certificates.

Why OpenPGP certificates? OpenPGP already has a trust model built-in, and the tools provided here can leverage existing trust paths that the user may already have (e.g. by using [OpenPGP CA](https://openpgp-ca.org/)).

Basically, the project aims to remove the need for the following prompts:

```
$ ssh user@example.com
The authenticity of host 'example.com (93.184.216.34)' can't be established.
ED25519 key fingerprint is SHA256:JxbmpZ1ESvTv0MEltlAbuiyb/GlVGpxbEq7JUGEqhPM.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

The usual reaction to this request is trusting on first use (`yes`) but this [can lead to security issues](https://www.agwa.name/blog/post/why_tofu_doesnt_work).
SSH OpenPGP Authenticator provides stronger guarantees that the SSH server is genuine while at the same time reducing the number of prompts.

## Installation

Components can be installed through `cargo`:

```sh
cargo install sshd-openpgp-auth # admin
cargo install ssh-openpgp-auth  # user
```

The preferred mechanism, due to updates and tighter integration with the operating system through manpages and shell completions, is using distribution packages:

  - ArchLinux: [`sshd-openpgp-auth`](https://archlinux.org/packages/extra/x86_64/ssh-openpgp-auth/), [`ssh-openpgp-auth`](https://archlinux.org/packages/extra/x86_64/ssh-openpgp-auth/).

## Technical details

Our solution consists of two components:

  - `sshd-openpgp-auth`: Host administrator-side tool that manages the "host certificate",
  - `ssh-openpgp-auth`: User-side tool that fetches host certificates, verifies trust chains, and configures the OpenSSH client to automatically trust all verified host keys.

This project works in conjunction with other free and open-source components:

  - [OpenSSH](https://www.openssh.com/): The administrator tool `sshd-openpgp-auth` interacts with the files used by the OpenSSH server-side component `sshd`, while the user-side tool `ssh-openpgp-auth` is used to extend the functionality of OpenSSH's client component `ssh`,

  - [PGP cert-d](https://sequoia-pgp.gitlab.io/pgp-cert-d/): Implemented as a common store of OpenPGP certificates, `pgp-cert-d` is also used by Sequoia-PGP's `sq`. Certificates discovered by `ssh-openpgp-auth` are automatically visible to Sequoia-PGP. Trust decisions expressed using `sq` subcommands are taken into account by `ssh-openpgp-auth`,

  - [Web Key Directory (WKD)](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/): This common, HTTP based directory structure for publishing and retrieving OpenPGP certificates is directly integrated with sshd-openpgp-auth (for generating a compatible directory structure) and ssh-openpgp-auth (for discovery of OpenPGP certificates on remote hosts).

```mermaid
graph TB
  subgraph administrator
    WKDS["WKD Server"]
    OSSHS["OpenSSH server"]
    SSHDTOOL{{"sshd-openpgp-auth"}} -- reads SSH host keys --> OSSHS
    SSHDTOOL -- writes host cert --> WKDS
  end

  subgraph user
    CERTD["PGP cert-d"]
    OSSHC["OpenSSH client"] -- connects via SSH --> OSSHS
    SSHTOOL{{"ssh-openpgp-auth"}} -- reads host cert --> WKDS
    SSHTOOL -- updates certs --> CERTD
    SSHTOOL -- supplies authenticated SSH keys --> OSSHC
  end
```

Note that an administrator may use separate hosts for the SSH server, the WKD server and the host on which one or more OpenPGP host certificates are administrated.

The client tool `ssh-openpgp-auth` can optionally enable stricter verification requirements such as DNSSEC proof of the certificate and Web of Trust authentication.

### Glossary

  - host certificate: OpenPGP certificate which represents a persistent identity of the host. This certificate will
    contain all SSH subkeys as an OpenPGP [Authentication subkeys](https://openpgp.dev/book/glossary.html#term-Authentication-Key-Flag),

  - cert-d: [internet draft](https://datatracker.ietf.org/doc/draft-nwjw-openpgp-cert-d/) specifying local directory for OpenPGP certificates,

  - WKD: Web Key Directory, remote directory of OpenPGP certificates.

### Implementation

When using the tools provided by this project, an OpenPGP certificate represents the identity of an SSH host.

Users may trust a host's OpenPGP certificate either explicitly or transitively (e.g., as part of an OpenPGP CA setup). All subkeys of the host's OpenPGP certificate are *Authentication keys*, one for each SSH host key. When the user's system has verified the host's OpenPGP certificate, the user's software transparently recognizes and trusts all SSH keys of that host, even when the remote host cycle some or all of its SSH keys. This works because the server operator links each of the host's SSH public keys to the host's OpenPGP certificate. The OpenPGP certificate acts as a Public Key Infrastructure (PKI) mechanism, which binds the public SSH host keys to a stable identity.

In setups where OpenPGP trust chains are already in use, the additional concern of authenticating SSH host identities is a straightforward and seamless extension, with these tools.

## Resources

More in-depth guides are located in the `docs` folder:

- [End-user's guide](docs/end-user.md)
- [OpenPGP CA integration](docs/openpgp-ca.md)
- [System administrator's guide](docs/sys-admin.md)

## Funding

This project is funded through [NGI Assure](https://nlnet.nl/assure), a fund established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more at the [NLnet project page](https://nlnet.nl/project/OpenPGP-OpenSSH).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGIAssure_tag.svg" alt="NGI Assure Logo" width="20%" />](https://nlnet.nl/assure)

## License

This project is licensed under either of:

  - [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0),
  - [MIT license](https://opensource.org/licenses/MIT).

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in this crate by you, as defined in the
Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
