#!/usr/bin/env -S just --working-directory . --justfile
# SPDX-FileCopyrightText: 2024 David Runge <dave@sleepmap.de>
# SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
# SPDX-License-Identifier: CC0-1.0
#
# Load project-specific properties from the `.env` file

set dotenv-load := true

# Whether to run ignored tests (set to "true" to run ignored tests)

ignored := "false"

# Runs all checks for currently checked files

# Since this is a first recipe it's being run by default.
check-files: spelling formatting lints dependencies licenses tests

# Faster checks need to be executed first for better UX.  For example
# codespell is very fast. cargo fmt does not need to download crates etc.

# Installs all tools required for development
install: install-packages install-tools

# Install development packages using pacman
install-packages:
    # Packages that are needed by this justfile are listed directly
    # Any extra packages are set in the `.env` file
    pacman -Syu --needed --noconfirm rustup cocogitto codespell reuse cargo-deny tangler rust-script $PACMAN_PACKAGES

# Installs any user tools required to run development tooling
install-tools:
    rustup default stable
    rustup component add clippy
    rustup toolchain install nightly
    rustup component add --toolchain nightly rustfmt

# Runs all tasks intended for the CI environment
ci: check-files e2e

# Checks common spelling mistakes
spelling:
    codespell

# Checks source code formatting
formatting:
    just --unstable --fmt --check
    # We're using nightly to properly group imports, see rustfmt.toml
    cargo +nightly fmt -- --check

# Lints the source code
lints:
    cargo clippy --all -- -D warnings

# Checks for issues with dependencies
dependencies:
    cargo deny check

# Checks licensing status
licenses:
    reuse lint

# Runs all unit tests. By default ignored tests are not run. Run with `ignored=true` to run only ignored tests
tests:
    {{ if ignored == "true" { "cargo test --all -- --ignored" } else { "cargo test --all" } }}

# Runs all end-to-end tests
e2e:
    #!/usr/bin/env bash
    set -euo pipefail
    for dir in *; do
        if [[ -d "$dir" && -f "$dir/Cargo.toml" && -f "$dir/README.md" ]]; then
            just test-readme "$dir"
        fi
    done

# Runs per project end-to-end tests found in a project README
test-readme project:
    #!/usr/bin/env bash
    if [[ -f "$EXE_DIR/{{ project }}" ]]; then
        printf "Precompiled version of %s found.\n" "{{ project }}"
        PATH="$EXE_DIR:$PATH"
    else
        printf "No precompiled version of %s found, installing...\n" "{{ project }}"
        cargo install --path {{ project }}
        if [[ -n "$CARGO_HOME" ]]; then
            PATH="$CARGO_HOME/bin:$PATH"
        else
            PATH="$HOME/.cargo/bin:$PATH"
        fi
    fi
    printf "PATH=%s\n" "$PATH"
    cd {{ project }} && tangler sh < README.md | bash -euxo pipefail -

# Adds git hooks (pre-commit, pre-push)
add-hooks:
    #!/usr/bin/env bash
    echo just check-files > .git/hooks/pre-commit
    chmod +x .git/hooks/pre-commit

    echo just check-commits > .git/hooks/pre-push
    chmod +x .git/hooks/pre-push

# Checks for commit messages
check-commits REFS='main..':
    #!/usr/bin/env bash
    set -euo pipefail
    for commit in $(git rev-list "{{ REFS }}"); do
      MSG="$(git show -s --format=%B "$commit")"
      CODESPELL_RC="$(mktemp)"
      git show "$commit:.codespellrc" > "$CODESPELL_RC"
      if ! grep -q "Signed-off-by: " <<< "$MSG"; then
        printf "Commit %s lacks \"Signed-off-by\" line.\n" "$commit"
        printf "%s\n" \
            "  Please use:" \
            "    git rebase --signoff main && git push --force-with-lease" \
            "  See https://developercertificate.org/ for more details."
        exit 1;
      elif ! codespell --config "$CODESPELL_RC" - <<< "$MSG"; then
        printf "The spelling in commit %s needs improvement.\n" "$commit"
        exit 1;
      elif ! cog verify "$MSG"; then
        exit 1;
      else
        printf "Commit %s formatting and spelling is good.\n" "$commit"
      fi
    done

# Create a CA cert and sign a TLS key with it
create-ci-tls-ca:
    #!/usr/bin/env bash
    set -euxo pipefail
    readonly org=localhost-ca
    readonly domain="${DOMAIN:-example.com}"
    readonly tls_dir="${TLS_DIR:-.}"

    mkdir -p "$tls_dir"
    openssl genpkey -algorithm RSA -out "$tls_dir/ca.key"
    openssl req -x509 -key "$tls_dir/ca.key" -out "$tls_dir/ca.crt" -subj "/CN=$org/O=$org"

    openssl genpkey -algorithm RSA -out "$tls_dir/$domain".key
    openssl req -new -key "$tls_dir/$domain.key" -out "$tls_dir/$domain.csr" -subj "/CN=$domain/O=$org"

    openssl x509 -req -in "$tls_dir/$domain.csr" -days 365 -out "$tls_dir/$domain.crt" -CA "$tls_dir/ca.crt" -CAkey "$tls_dir/ca.key" -CAcreateserial -extfile <(cat <<END
    basicConstraints = CA:FALSE
    subjectKeyIdentifier = hash
    authorityKeyIdentifier = keyid,issuer
    subjectAltName = DNS:$domain
    END
    )

# Trust a local Certificate Authority (CA). This target assumes elevated privileges to alter the system's trust store.
trust-ci-tls-ca:
    #!/usr/bin/env bash
    set -euxo pipefail
    readonly tls_dir="${TLS_DIR:-.}"

    trust anchor "$tls_dir/ca.crt"

# Prepare a test host. This target should be used in a container, as it is destructive to the host and assumes no running services and being run as root. It prepares the SSH configuration for root, so that ssh-openpgp-auth can be used as KnownHostsCommand and successful logins print a message and log the user out automatically.
prepare-ci-test-host:
    #!/usr/bin/env bash
    set -euxo pipefail
    readonly fqdn="${DOMAIN:-example.com}"

    printf "127.0.0.1 %s\n" "$fqdn" >> /etc/hosts

    printf "#!/bin/bash\nprintf 'Logged in over ssh!\nLogging out...\n'\nexit 0\n" > /usr/local/bin/rootlogin
    chmod +x /usr/local/bin/rootlogin
    printf "Match user root\n  ForceCommand /usr/local/bin/rootlogin\n" > /etc/ssh/sshd_config.d/10-rootlogin.conf
    ssh-keygen -A
    /usr/bin/sshd

    mkdir -p /root/.ssh
    ssh-keygen -q -f /root/.ssh/id_ed25519 -N ""
    cat /root/.ssh/*.pub > /root/.ssh/authorized_keys
    cat /root/.ssh/authorized_keys

# Use miniserve to expose a Web Key Directory (WKD) locally over TLS. This assumes being able to use port 443 (which mostly requires root).
host-ci-wkd-dir:
    #!/usr/bin/env bash
    set -euxo pipefail
    readonly tls_dir="${TLS_DIR:-.}"
    readonly fqdn="${DOMAIN:-example.com}"
    readonly wkd_dir="${WKD_DIR:-wkd}"

    if [[ ! -d "$wkd_dir" ]]; then
        mkdir -p "$wkd_dir"
    fi

    miniserve --hidden --tls-cert "$tls_dir/$fqdn.crt" --tls-key "$tls_dir/$fqdn.key" --interfaces 127.0.0.1 --port 443 "$wkd_dir" &

# Use ssh to connect to a domain. Use SHOULD_SUCCEED environment variable (defaults to 1) to indicate whether the SSH connection should succeed.
connect-ci-ssh:
    #!/usr/bin/env bash
    set -euxo pipefail
    readonly fqdn="${DOMAIN:-example.com}"
    readonly soa_auth_options="${SOA_AUTH_OPTIONS:---verbose}"
    readonly should_succeed="${SHOULD_SUCCEED:-1}"

    if [[ -n "$EXE_DIR" ]]; then
        PATH="$EXE_DIR:$PATH"
        exe_dir="${EXE_DIR}/"
    # allow providing just the command name if `EXE_DIR` is an empty string
    else
        exe_dir=""
    fi

    # prepare the configuration for the target host
    printf "Host %s\n  KnownHostsCommand %sssh-openpgp-auth authenticate %s %%H\n" "$fqdn" "$exe_dir" "$soa_auth_options" > /root/.ssh/config
    cat /root/.ssh/config

    if (( should_succeed == 1 )); then
        if ! ssh "$fqdn" -vv ; then
            printf "SSH connection to %s failed but should have succeeded!\n" "$fqdn"

            # run ssh-openpgp-auth authenticate again with the same options in case we are failing to show its output
            read -ra auth_options <<<"$soa_auth_options"
            set +e
            ssh-openpgp-auth authenticate "${auth_options[@]}" "$fqdn"
            set -e

            sq pki list || true
            sq cert export --email "ssh-openpgp-auth@$fqdn" | sq inspect --certifications

            exit 1
        fi
    else
        if ssh "$fqdn" -vv ; then
            printf "SSH connection to %s succeeded but should have failed!\n" "$fqdn"

            # run ssh-openpgp-auth authenticate again with the same options in case we are failing to show its output
            read -ra auth_options <<<"$soa_auth_options"
            ssh-openpgp-auth authenticate "${auth_options[@]}" "$fqdn"

            sq pki list || true
            sq cert export --email "ssh-openpgp-auth@$fqdn" | sq inspect --certifications

            exit 1
        fi
    fi

# Export to created OpenPGP host certificate to WKD and show the resulting directory structure
export-ci-to-wkd:
    #!/usr/bin/env bash
    set -euxo pipefail
    readonly fqdn="${DOMAIN:-example.com}"
    readonly wkd_dir="${WKD_DIR:-wkd}"

    sshd-openpgp-auth export --wkd-type direct --output-dir "$wkd_dir" "$fqdn"
    sshd-openpgp-auth export --output-dir "$wkd_dir" "$fqdn"
    tree -a "$wkd_dir"

# Setup OpenPGP host certificates and export to WKD to allow tests for valid and revoked SSH host keys. PGPKI based authentication is done using a local trusted introducer (CA).
test-ci-local-trusted-introducer:
    #!/usr/bin/env bash
    set -euxo pipefail
    readonly fqdn="${DOMAIN:-example.com}"
    readonly wkd_dir="${WKD_DIR:-wkd}"
    readonly cert_dir="${CERT_DIR:-.}"
    readonly custom_ca_dir="$cert_dir/custom_ca"
    readonly openpgp_dir="${SOA_OPENPGP_DIR:-/var/lib/sshd-openpgp-auth}"

    if [[ -n "$EXE_DIR" ]]; then
        PATH="$EXE_DIR:$PATH"
    fi

    # create a new TSK to serve as local trust anchor
    mkdir -vp "$custom_ca_dir"
    sq key generate --userid 'Domain CA <openpgp-ca@example.com>' --output "$custom_ca_dir/local_trust_anchor.tsk"
    # import certificate of local trust anchor to cert store
    sq key extract-cert "$custom_ca_dir/local_trust_anchor.tsk" | sq cert import
    # set the newly added certificate up as trusted introducer (CA)
    trust_anchor_fingerprint="$(sq inspect "$custom_ca_dir/local_trust_anchor.tsk" 2>&1 | sed -ne 's/.*Fingerprint: \(.*\)/\1/p')"
    sq inspect --certifications "$custom_ca_dir/local_trust_anchor.tsk"
    printf "Local trust anchor fingerprint: %s\n" "$trust_anchor_fingerprint"
    sq pki link add --all --ca "$fqdn" "$trust_anchor_fingerprint"

    # authentication should fail, because the client can not retrieve a certificate yet
    SOA_AUTH_OPTIONS="--verbose" SHOULD_SUCCEED=0 just connect-ci-ssh
    SOA_AUTH_OPTIONS="--verbose --verify-wot" SHOULD_SUCCEED=0 just connect-ci-ssh

    # prepare an expired TSK to manage SSH host keys as authentication subkeys
    sshd-openpgp-auth init --time "$(date -d 'yesterday' -Iseconds)" --expiry 1 "$fqdn"
    sshd-openpgp-auth add
    sshd-openpgp-auth list
    sq inspect --certifications "$openpgp_dir/"*.tsk

    host_fingerprint="$(sshd-openpgp-auth list | grep '🔑' | cut -f 2 -d ' ')"
    key_file="$openpgp_dir/${host_fingerprint}.tsk"
    cert_file_name="${host_fingerprint}.pgp"

    printf "certificate %s\n" "$cert_file_name"
    printf "fingerprint %s\n" "$host_fingerprint"

    # export to WKD
    just export-ci-to-wkd

    # authentication should fail, because the retrieved certificate is expired
    SOA_AUTH_OPTIONS="--verbose" SHOULD_SUCCEED=0 just connect-ci-ssh
    SOA_AUTH_OPTIONS="--verbose --verify-wot" SHOULD_SUCCEED=0 just connect-ci-ssh

    # extend the expiry of the OpenPGP host certificate and export to WKD again
    sshd-openpgp-auth extend
    just export-ci-to-wkd

    # authentication should succeed, because the the expired certificate is updated
    SOA_AUTH_OPTIONS="--verbose" SHOULD_SUCCEED=1 just connect-ci-ssh
    # authentication should fail, because the extended certificate is not yet certified
    SOA_AUTH_OPTIONS="--verbose --verify-wot" SHOULD_SUCCEED=0 just connect-ci-ssh

    # use local trust anchor to certify the host's certificate
    sq pki certify --output "$custom_ca_dir/$cert_file_name" "$custom_ca_dir/local_trust_anchor.tsk" "$key_file" "<ssh-openpgp-auth@$fqdn>"
    sq inspect --certifications "$custom_ca_dir/$cert_file_name"

    # merge certificate containing certification into host TSK and export to WKD again
    sshd-openpgp-auth merge "$custom_ca_dir/$cert_file_name"
    just export-ci-to-wkd

    # remove local cert-d store and re-import local trusted introducer certificate
    # NOTE: here we only have to do this because ssh-openpgp-auth does not force fetch from WKD: https://codeberg.org/wiktor/ssh-openpgp-auth/issues/89
    rm -rv "$PGP_CERT_D"
    sq key extract-cert "$custom_ca_dir/local_trust_anchor.tsk" | sq cert import
    sq pki link add --all --ca "$fqdn" "$trust_anchor_fingerprint"

    # authentication should succeed
    SOA_AUTH_OPTIONS="--verbose" SHOULD_SUCCEED=1 just connect-ci-ssh
    SOA_AUTH_OPTIONS="--verbose --verify-wot" SHOULD_SUCCEED=1 just connect-ci-ssh

    # hard revoke all subkeys and re-export them to WKD
    sshd-openpgp-auth revoke --reason "compromised" --message "Whoops!" --all
    sshd-openpgp-auth list
    just export-ci-to-wkd

    # authentication should still succeed, because the local certificate is still valid and is not fetched again
    SOA_AUTH_OPTIONS="--verbose" SHOULD_SUCCEED=1 just connect-ci-ssh
    SOA_AUTH_OPTIONS="--verbose --verify-wot" SHOULD_SUCCEED=1 just connect-ci-ssh

    # remove local cert-d store and re-import local trusted introducer certificate
    rm -rv "$PGP_CERT_D"
    sq key extract-cert "$custom_ca_dir/local_trust_anchor.tsk" | sq cert import
    sq pki link add --all --ca "$fqdn" "$trust_anchor_fingerprint"

    # authentication should fail, because the authentication subkeys are revoked in the updated certificate
    SOA_AUTH_OPTIONS="--verbose" SHOULD_SUCCEED=0 just connect-ci-ssh
    SOA_AUTH_OPTIONS="--verbose --verify-wot" SHOULD_SUCCEED=0 just connect-ci-ssh

# Run a full integration test against a setup validating via WKD and validating the PKI based on a local trust anchor acting as CA for the domain
test-ci-local-trust-anchor: create-ci-tls-ca trust-ci-tls-ca prepare-ci-test-host host-ci-wkd-dir test-ci-local-trusted-introducer

# Fixes common issues. Files need to be git add'ed
fix:
    #!/usr/bin/env bash
    if ! git diff-files --quiet ; then
        echo "Working tree has changes. Please stage them: git add ."
        exit 1
    fi

    codespell --write-changes
    just --unstable --fmt
    cargo clippy --fix --allow-staged

    # fmt must be last as clippy's changes may break formatting
    cargo +nightly fmt

render-script := '''
    //! ```cargo
    //! [dependencies]
    //! pkg = { path = "PKG", package = "PKG" }
    //! clap_allgen = "0.1.0"
    //! ```

    fn main() -> Result<(), Box<dyn std::error::Error>> {
        clap_allgen::render_KIND::<pkg::Commands>(
            &std::env::args().collect::<Vec<_>>()[1],
        )?;
        Ok(())
    }
'''

# Render `manpages` or `shell_completions` (`kind`) of a given package (`pkg`) to directory given by `output`.
generate kind pkg output:
    sed 's/PKG/{{ pkg }}/g;s/KIND/{{ kind }}/g' > .script.rs <<< '{{ render-script }}'
    rust-script .script.rs '{{ output }}'
    rm --force .script.rs
